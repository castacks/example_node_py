import rospy

from base_py import BaseNode


class MyNode(BaseNode):
    def __init__(self):
        super(MyNode, self).__init__('mynode')

    def initialize(self):
        rospy.loginfo('initializing a bunch of stuff')

        private_param = self.get_private_param('private_param')
        global_param = self.get_param('global_param')
        rospy.loginfo('global_param: %s', global_param)
        rospy.loginfo('private_param: %s', private_param)
        return True

    def execute(self):
        # do work
        rospy.loginfo('work')
        return True

    def shutdown(self):
        rospy.loginfo("I'm done here")
